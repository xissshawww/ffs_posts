---
title: 'DENUDE'
date: 2021-03-22 11:43:46
tags:
    - lrd
---

关于裸身集会名字中的denude，如果水谷本人真的很喜欢洛尔迦，我推测这个取名的灵感可能来源于西班牙诗人洛尔迦本人非常着迷的duende，代表某种精灵。

新牛津字典的解释：

> 1.A ghost, an evil spirit; 2.Inspiration, magic, fire.  

Random House字典的解释是：
> 1.A goblin, demon, spirit; 2.Charm, magnetism.

以洛尔迦的理解，duende与音乐有着很大的联系：

> 整个安达卢西亚......人们不断谈论duende，并在它出现时以坚定的本能去辨识它。出色的弗拉门戈歌手El Lebrijano说：“当我用duende唱歌时，没有人能比得上我......”Manuel Torres在听Falla演奏自己的《Nocturno del Genaralife》时，他比我所认识的人都更有文化，他发表了精彩的宣讲：“所有拥有黑暗声音（dark sounds）的人都有duende。”没有别的真理了。
>
> 这些黑暗声音是神秘的，它的根深深植根于我们所有人都知道的肥沃的壤土中，被我们所有人忽视了，但我们从中获得了艺术的真实......
>
> 因此，duende是一种力量，而不是一种行为，它是一种斗争，而不是一个概念。我听到一位老吉他大师说：“duende不在喉咙里；duende在脚底隆隆作响。”这意味着这不是能力问题，而是真正的生活形式问题；血液问题；古代文化问题；创造性行动问题。
>
> ——[The Theory and Function of Duende](https://www.poetryintranslation.com/PITBR/Spanish/LorcaDuende.php)，洛尔迦，[（duendedrama.org）](https://www.duendedrama.org/about-duende/)

摘《死在黎明》译者王家新前言：

> 在吉卜赛传统中，“魔灵”可以让表演者进入“着魔”“迷狂”的状态，并把观众也带入其中。洛尔迦所诉求的“魔灵”，正是这样一种存在。在美国诗人、洛尔迦诗歌的译者W.S.默温看来，正是在“魔灵”的掌握中，洛尔迦的诗歌“有着它的最纯粹的形式、音调、生命、存在，那就是我从一开始就一直在倾听的。”

裸身集会在日本被称为*Hadaka no rariizu*，*Hadaka no*被翻译成*nu*或*dénudé*，*裸のラリーズ*中的*ラリーズ*（发音ràríꜜrù）是日语俚语，来源于日语动词“ラリる”，意思是“嗑high了”（[提示来源](https://fleurmach.com/2016/03/27/les-rallizes-denudes/)），并且*ラリーズ*的[英文翻译](https://en.bab.la/dictionary/japanese-english/%E3%83%A9%E3%83%AA%E3%83%BC)Rallys也有“集合、集会、[鳩合](https://en.bab.la/dictionary/japanese-english/鳩合)、[反発](https://en.bab.la/dictionary/japanese-english/反発)”的意思。

+-----

日语：裸のラリーズ（Hadaka no rariizu）

中文：裸身集会

通用：Les rallizes dénudés

+-----

les来源于：法语中的**定冠词（l'article défini）**，Les 是复数形式（Le 是阳性单数形式，La 是阴性单数形式），后跟复数名词（无论阴阳性），例如 les femmes 那些女人。

rallizes来源于：rariizu的发音，假写为英文

dénudés来源于：duende + naked (lunch) = de + nude + s?